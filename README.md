# userChrome.css

The userChrome is a cascading style sheet file that can be used to custom some web browsers.
This one was made for FireFox.

## Installation
* Go to your FireFox profile directory ([guide](https://support.mozilla.org/en-US/kb/profiles-where-firefox-stores-user-data))
* Create a new folder named `chrome`
* Inside it, just put this file, and restart FireFox

### Tips
* You can access the Browser Toolbox following this ([guide](https://developer.mozilla.org/en-US/docs/Tools/Browser_Toolbox)) from Mozilla

## Screenshot
![FireFox Screenshot](userChrome.png)